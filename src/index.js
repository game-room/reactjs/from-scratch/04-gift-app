import React from 'react';
import ReactDOM from 'react-dom';
import GifExpertApp from './GifExpertApp';
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <GifExpertApp defaultCategories={ ['Sakura Card Captor', 'Totoro'] } />
  </React.StrictMode>,
  document.getElementById('root')
);
